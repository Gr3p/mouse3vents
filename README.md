# mouse3vents.js

mouse3vents.js is a **lightweight** module that I implemented to learn how to create a C++ addon for nodejs.

It allows to trigger mouse events such **right** / **left** click and mouse move.


It works actually **only on Windows**.

*tested on* 
- win10
- nodejs v10.15.3

## Install

```bash
npm i mouse3vents
```

## Docs

The module has following methods:

### .doubleClick()

Trigger mouse left double click event

### .getPosition()

Get coordinates of mouse
 
    @returns {Object} Position
    @property {number} x - The X Coordinate
    @property {number} y - The Y Coordinate


### .leftClick()

Trigger mouse left click event

### .leftClickDown()

Trigger mouse left click down event

### .leftClickUp()

Trigger mouse left click up event

### .moveTo(x, y)

Move mouse to coordinates.
 
| Param  | Type                | Description  |
| ------ | ------------------- | ------------ |
|x | <code>Number</code> |  X-Coordinate |
|y| <code>Number</code> |  Y-Coordinate  |


### .rightClick()

Trigger mouse right click event

### .rightClickDown()

Trigger mouse right click down event

### .rightClickUp()

Trigger mouse right click up event
 

## Examples

```javascript
const MouseEvents = require('mouse3vents');

MouseEvents.moveTo(100,500);    // move on coordinates X:100 and Y:500

MouseEvents.rightClick();   //trigger a right click event
```

More examples in [examples](https://gitlab.com/Gr3p/mouse3vents/tree/master/examples) folder.