#include <node.h>
#include <Windows.h>
#include <io.h>

namespace MouseEvents {

using v8::Context;
using v8::Exception;
using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::NewStringType;
using v8::Number;
using v8::Object;
using v8::String;
using v8::Value;

// Input arguments are passed using the
// const FunctionCallbackInfo<Value>& args struct

void moveTo(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  // Check the number of arguments passed.
  if (args.Length() < 2) {
    // Throw an Error that is passed back to JavaScript
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate,
                            "Wrong number of arguments",
                            NewStringType::kNormal).ToLocalChecked()));
    return;
  }

  // Check the argument types
  if (!args[0]->IsNumber() || !args[1]->IsNumber()) {
    isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate,
                            "Wrong arguments",
                            NewStringType::kNormal).ToLocalChecked()));
    return;
  }

  // Assign values
  double x = args[0].As<Number>()->Value();
  double y = args[1].As<Number>()->Value();

  SetCursorPos(x, y);
}

void rightClick(const FunctionCallbackInfo<Value>&) {
  mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
  mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
}

void rightClickUp(const FunctionCallbackInfo<Value>&) {
  mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
}

void rightClickDown(const FunctionCallbackInfo<Value>&) {
  mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
}

void doubleClick(const FunctionCallbackInfo<Value>&) {
  mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
  mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
  mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
  mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
}

void leftClick(const FunctionCallbackInfo<Value>&) {
    mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
    mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
}

void leftClickDown(const FunctionCallbackInfo<Value>&) {
    mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
}

void leftClickUp(const FunctionCallbackInfo<Value>&) {
    mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
}

void getPosition(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  POINT curPos;
  BOOL result = GetCursorPos(&curPos);

    if (result) {
        float x = curPos.x;
        float y = curPos.y;;

        // creating new Object
        Local<Object> obj = Object::New(isolate);

        //set attribute 'x'
        obj->Set(context,
                   String::NewFromUtf8(isolate,
                                       "x",
                                       NewStringType::kNormal).ToLocalChecked(),
                                       Number::New(isolate, x))
                   .FromJust();

        //set attribute 'y'
        obj->Set(context,
                   String::NewFromUtf8(isolate,
                                       "y",
                                       NewStringType::kNormal).ToLocalChecked(),
                                       Number::New(isolate, y))
                   .FromJust();

            args.GetReturnValue().Set(obj);
    }
    else
    {
      // Throw an Error that is passed back to JavaScript
          isolate->ThrowException(Exception::TypeError(
              String::NewFromUtf8(isolate,
                                  "No Mouse was found!",
                                  NewStringType::kNormal).ToLocalChecked()));
   }
}


void Init(Local<Object> exports) {
  NODE_SET_METHOD(exports, "doubleClick", doubleClick);
  NODE_SET_METHOD(exports, "getPosition", getPosition);
  NODE_SET_METHOD(exports, "leftClick", leftClick);
  NODE_SET_METHOD(exports, "leftClickDown", leftClickDown);
  NODE_SET_METHOD(exports, "leftClickUp", leftClickUp);
  NODE_SET_METHOD(exports, "moveTo", moveTo);
  NODE_SET_METHOD(exports, "rightClick", rightClick);
  NODE_SET_METHOD(exports, "rightClickDown", rightClickDown);
  NODE_SET_METHOD(exports, "rightClickUp", rightClickUp);
}

NODE_MODULE(NODE_GYP_MODULE_NAME, Init)

}