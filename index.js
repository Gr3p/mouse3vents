module.exports = require('./src/MouseEvents');

/**
 * <>.doubleClick()
 * Trigger mouse left double click event
 */

/**
 * <>.getPosition()
 * Get coordinates of mouse
 * @returns {Object} Position
 * @property {number} x - The X Coordinate
 * @property {number} y - The Y Coordinate
 */

/**
 * <>.leftClick()
 * Trigger mouse left click event
 */

/**
 * <>.leftClickDown()
 * Trigger mouse left click down event
 */

/**
 * <>.leftClickUp()
 * Trigger mouse left click up event
 */

/**
 * <>.moveTo()
 * Move Mouse to coordinates
 * @param {Number} x - X-Coordinate
 * @param {Number} y - Y-Coordinate
 */

/**
 * <>.rightClick()
 * Trigger mouse right click event
 */

/**
 * <>.rightClickDown()
 * Trigger mouse right click down event
 */

/**
 * <>.rightClickUp()
 * Trigger mouse right click up event
 */