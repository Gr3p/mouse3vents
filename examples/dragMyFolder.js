const ME = require('../index');

function sleep(ms) {
    return new Promise(function (resolve) {
        setTimeout(resolve, ms)
    })
}

async function dragMyFolder() {
    ME.moveTo(296, 351);

    ME.leftClickDown();

    ME.moveTo(296, 600);
    await sleep(2000);

    ME.leftClickUp();

    return true;
}

dragMyFolder()
    .then(function () {
        console.log('Done!');
    });



