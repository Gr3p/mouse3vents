const {moveTo} = require('mouse3vents');

async function sinWaveAnimation() {
    const twoPI = Math.PI * 2.0;
    const screenSize = {height: 1080, width: 1900};
    const height = (screenSize.height / 2);
    const width = screenSize.width;

    function sleep(ms) {
        return new Promise(function (resolve) {
            setTimeout(resolve, ms)
        })
    }

    for (let i = 0; i < width; i++) {
        await sleep(60 / 1000);
        moveTo(i, height * Math.sin((twoPI * i) / width) + height);
    }
}

sinWaveAnimation()
    .then(function () {
        console.log('Done!')
    });