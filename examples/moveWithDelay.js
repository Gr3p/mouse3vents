/**
 * Example of moving mouse on monitor with a certain delay
 *
 */

const {moveTo} = require('mouse3vents');

function sleep(ms) {
    return new Promise(function (resolve) {
        setTimeout(resolve, ms)
    })
}

async function moveWithDelay(delay) {
    for (let i = 0; i < 1000; i++) {
        await sleep(delay);
        moveTo(i, 500);
    }
    return true;
}

moveWithDelay(60 / 1000)
    .then(function () {
        console.log('Done!');
    });